import java.io.IOException;
import java.net.UnknownHostException;

import telran.net.TcpClientJava;

public class TcpClient extends TcpClientJava{

	public TcpClient(String hostName, int port) throws UnknownHostException, IOException {
		super(hostName, port);
	}

}
