import telran.net.TcpClientJava;
import telran.view.ConsoleInputOutput;
import telran.view.InputOutput;
import telran.view.Item;
import telran.view.Menu;

public class ClientApp {
	static InputOutput inputOutput = new ConsoleInputOutput();
	public static void main(String[] args) throws Exception {
		
		TcpClientJava tcpClient = new TcpClient("localhost", 5000);
		Item[] items = {
			new HireEmployeeItem(inputOutput, tcpClient)
		};
		Menu menu = new Menu(items, inputOutput);
		menu.runMenu();
	}
}