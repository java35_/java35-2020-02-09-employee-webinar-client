package telran.view;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Function;

public interface InputOutput {
	String inputString(String prompt);
	void output(Object obj);
	
	default Integer inputInteger(String prompt)
	{
		return inputObject(prompt,"it's not a number",
				s->{
					try {
						Integer res = Integer.parseInt(s);
						return res;
					} catch (NumberFormatException e) {
						return null;
					}
				});
	}
	default <R> R inputObject(String prompt, String errorPrompt, 
			Function<String, R> mapper)
	{
		while(true)
		{
			String text = inputString(prompt);
			if(text==null) return null;
			R res = mapper.apply(text);
			if(res!=null) return res;
			outputLine(errorPrompt);
		}
		
	}
	default void outputLine(Object obj)
	{
		output(obj.toString()+"\n");
	}
	//===============================
	default Double inputDouble(String prompt){
		//TODO
		return null;
	}
	default Long inputLong(String prompt)
	{
		//TODO
		return null;
	}
	default String inputString(String prompt, List<String> options)
	{
		//TODO
		return null;
	}
	default LocalDate inputDate(String prompt, String format)
	{
		//TODO
		return null;
	}
	default Integer inputInteger(String prompt,int from,int to)
	{
		return inputObject(prompt,"it's not a number",
				s->{
					try {
						Integer res = Integer.parseInt(s);
						if (res < from || res > to)
							throw new NumberFormatException();
						return res;
					} catch (NumberFormatException e) {
						return null;
					}
				});
	}
}
