package api.dto;

import java.io.Serializable;

public class EmployeeDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int id;
	String name;
	String companyName;
	int salary;
	
	public EmployeeDto(int id, String name, String companyName, int salary) {
		this.id = id;
		this.name = name;
		this.companyName = companyName;
		this.salary = salary;
	}
	
	@Override
	public String toString() {
		return "id=" + id + ", name=" + name + 
				", companyName=" + companyName + 
				", salary=" + salary;
	}
}
