import api.dto.EmployeeDto;
import telran.net.ResponseJava;
import telran.net.TcpClientJava;
import telran.view.InputOutput;
import telran.view.Item;

public class HireEmployeeItem implements Item {

	InputOutput io;
	TcpClientJava tcpClient;
	
	public HireEmployeeItem(InputOutput io, TcpClientJava tcpClient) {
		this.io = io;
		this.tcpClient = tcpClient;
	}
	
	@Override
	public String displayedName() {
		return "Hire employee";
	}

	@Override
	public void perform() {
		int id 				= io.inputInteger("Input id");
		String name 		= io.inputString("Input name");
		String companyName 	= io.inputString("Input company name");
		int salary 			= io.inputInteger("Input salary", 4800, 50000);
		EmployeeDto employeeDto = new EmployeeDto(id, name, companyName, salary);
		boolean res = tcpClient.sendRequest("Hire employee", employeeDto);
		System.out.println(res);
	}

}
